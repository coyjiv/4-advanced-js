const root = document.getElementById("root");
const BASE_URL = "https://ajax.test-danit.com/api/swapi/films";

class Film {
  request(url) {
    return axios.get(url);
  }
  getInfo() {
    this.request(BASE_URL).then(
      function ({ data }) {
        const globalUl = document.createElement("ul");
        for (let index = 0; index < data.length; index++) {
          const filmObj = data[index];
          const smallUl = document.createElement("ul");
          const filmName = document.createElement("li");
          filmName.textContent = filmObj.name;
          const filmEpisode = document.createElement("li");
          filmEpisode.textContent = "Episode " + filmObj.episodeId;
          const filmDesc = document.createElement("li");
          filmDesc.textContent = filmObj.openingCrawl;
          const charactersUl = document.createElement("ul");
          const charactersTopic = document.createElement("li");
          charactersTopic.textContent = "Characters:";
          charactersUl.append(charactersTopic);
          filmObj.characters.forEach((element) => {
            this.request(element).then(function (data) {
              const loader = document.querySelector(".absolute-anim");
              loader.classList.remove("hide");
              if ((data.status = 200)) {
                loader.classList.add("hide");
              }
              const liChar = document.createElement("li");
              // console.log(data.data);
              liChar.textContent = data.data.name;
              const parInfo = document.createElement("p");
              for (const key in data.data) {
                if (Object.hasOwnProperty.call(data.data, key)) {
                  const element = data.data[key];
                  parInfo.innerHTML += key + " : " + element + "<br>";
                }
              }
              liChar.append(parInfo);
              charactersUl.append(liChar);
            });
          });
          smallUl.append(filmName, filmEpisode, filmDesc, charactersUl);
          globalUl.append(smallUl);
        }
        root.append(globalUl);
      }.bind(this)
    );
  }
}

const film = new Film();
film.getInfo();
